import { render } from 'react-dom';
import * as React from 'react';
import App from './containers/App';

render(<App />, document.getElementById('app'));
