import styled from 'styled-components';
import * as React from 'react';
import * as PropTypes from 'prop-types';

const HeaderWrapper = styled.div`
  box-sizing: border-box;
  background-color: #4688f1;
  height: 64px;
  color: white;
  box-shadow: 0 2px 13px 0 rgba(0, 0, 0, 0.5);
  
  
`;

const HeaderContent = styled.div`
  height: 100%;
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-end;
  align-content: center;
  
  padding: 0 20px 0 20px;
`;

const Header: React.FunctionComponent = ({ children }) => (
  <HeaderWrapper>
    <HeaderContent>
      {children}
    </HeaderContent>
  </HeaderWrapper>
);

Header.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Header;
