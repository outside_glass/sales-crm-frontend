import * as React from 'react';
import * as PropTypes from 'prop-types';
import styled from 'styled-components';
import Header from './Header';
import Content from './Content';
import Link from './Link';

const FlexContainer = styled.div`
  display: flex;
  flex-direction: column;
  
  height: 100vh;
`;

const Page: React.FunctionComponent = ({ children }) => (
  <FlexContainer>
    <Header>
      <Link href="/login">Войти</Link>
    </Header>
    <Content>
      {children}
    </Content>
  </FlexContainer>
);

Page.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]).isRequired,
};

export default Page;
