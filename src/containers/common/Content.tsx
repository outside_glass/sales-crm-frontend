import styled from 'styled-components';

const Content = styled.div`
  flex-grow: 1;
  
  overflow-y: auto;
  
  height: 100%;
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
`;

export default Content;
