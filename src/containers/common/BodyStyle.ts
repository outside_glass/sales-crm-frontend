import { createGlobalStyle } from 'styled-components';

const BodyStyle = createGlobalStyle`
  body {
    margin: 0;
    font: 14px sans-serif;
    
  }
`;

export default BodyStyle;
