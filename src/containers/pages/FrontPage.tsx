import * as React from 'react';
import styled from 'styled-components';
import Page from '../common/Page';

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-flow: column wrap;
  
  height: 100%;
`;

const Title = styled.h1`
`;

const Description = styled.div`
  font-size: 1.5em;
  width: 50%;
`;

const ImageWrapper = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-around;
  
  width: 100%;
  
  margin: 1em 0;
`;

const FrontPage: React.FunctionComponent = () => (
  <Page>
    <Wrapper>
      <Title>Sales CRM - лучшая CRM на всем локалхосте</Title>
      <Description>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Nullam euismod tempor varius. Maecenas euismod ex ut ultricies fringilla.
        Duis ut ipsum vel justo mattis molestie. Nulla aliquet erat non arcu interdum hendrerit.
        Vivamus ultricies at enim ac tincidunt. Nulla facilisi.
        Integer sodales, neque vel blandit molestie, quam lacus porta purus.
      </Description>
      <ImageWrapper>
        <img src="https://via.placeholder.com/500x250/EEEEEE" />
        <img src="https://via.placeholder.com/500x250/EEEEEE" />
      </ImageWrapper>
    </Wrapper>
  </Page>
);

export default FrontPage;
