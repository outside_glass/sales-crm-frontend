import * as React from 'react';
import { ReactElement } from 'react';
import BodyStyle from './common/BodyStyle';
import FrontPage from './pages/FrontPage';

const App = (): ReactElement => (
  <>
    <BodyStyle />
    <FrontPage />
  </>
);

export default App;
