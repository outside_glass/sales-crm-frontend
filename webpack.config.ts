import * as path from 'path';
import { Configuration } from 'webpack';

const DotenvWebpackPlugin = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

require('dotenv').config();

const isProduction = process.env.ENV === 'production';

const sourcePath = path.resolve(__dirname, 'src');
const entry = path.resolve(sourcePath, 'index.tsx');

const config: Configuration = {
  entry: {
    index: entry,
  },

  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].[hash].js',
    publicPath: '/',
  },

  mode: process.env.ENV as Configuration['mode'],

  // Enable sourcemaps for debugging webpack's output.
  devtool: process.env.ENV === 'production' ? 'cheap-source-map' : 'inline-source-map',

  resolve: {
    // Add '.ts' and '.tsx' as resolvable extensions.
    extensions: ['.ts', '.tsx', '.js'],
    modules: [
      sourcePath,
      'node_modules',
    ],
  },

  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'ts-loader',
          },
        ],
      },
      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader',
      },
    ],
  },

  plugins: [
    new DotenvWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: 'public/index.html',
      inject: 'body',
      hash: true,
    }),
  ],

  devServer: {
    contentBase: './dist',
    host: 'localhost',
    port: 3000,
    compress: isProduction,
    publicPath: '/',
    watchOptions: {
      ignored: /node_modules/,
    },
    historyApiFallback: {
      index: path.resolve('public', 'index.html'),
    },
  },
};

export default config;
